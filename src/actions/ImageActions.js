import {
  ADD_COMMENT,
  ADD_IMAGE,
} from '../constants/ActionTypes';

export function addImage(image) {
  return {
    type: ADD_IMAGE,
    payload: image
  };
}

export function addComment(comment) {
  return {
    type: ADD_COMMENT,
    payload: comment
  };
}