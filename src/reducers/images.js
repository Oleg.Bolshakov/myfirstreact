import {
  ADD_COMMENT,
  ADD_IMAGE,
} from '../constants/ActionTypes';

const initialState = {
  image: {
    id: null,
    url: null,
    comments: []
  },
  images: []
}

export default function images(state = initialState, action) {
  
  let image = state.images.find(image => {return Number(image.id) === Number(action.payload.id) });

  switch (action.type) {
    case ADD_IMAGE:
      if (image) {
        image.url = action.payload.url;
        if (Array.isArray(action.payload.comments)) {
          let comments = action.payload.comments.filter(comment => {return !image.comments.some(c => Number(c.id)===Number(comment.id))});
          image.comments = image.comments.concat(comments);
          image.comments.sort(compareDate);
        };
      } else {
        state.images.push(action.payload);
        image = state.images[state.images.length - 1];
      };
      return Object.assign({}, state, {image: image});
    
    case ADD_COMMENT:
      let max = Math.max( ...image.comments.map(comment=>{return comment.id}) );
      if (max < 0) {max = 0};
      image.comments.push({
        id: ++max,
        text: action.payload.text,
        date: Date.now()
      });
      return Object.assign({}, state, {image: image});

    default:
      return state;
  }
};

function compareDate (commentA, commentB) {
  return new Date(commentA.date) - new Date(commentB.date);
}