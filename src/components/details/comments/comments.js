import React, { Component } from 'react';
import './comments.css';

import moment from 'moment';

export default class Comments extends Component {

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const comments = Array.isArray(this.props.comments) ? this.props.comments : [];
    const commentList = comments.map((comment) => {
      let date = moment(comment.date).format('YYYY-MM-DD HH:mm:ss');
      return (
        <div className="comment" key={comment.id.toString()}>
          <div className="datetime">{date}</div>
          <div className="text">{comment.text}</div>
        </div>
      );
    });
    if (commentList.length > 0) {
      return (
        <div className="comment-container">
          {commentList}
        </div>
      );
    } else {
      return (
        <div className="comment-container">
          <p>No comments yet</p>
        </div>
      );
    };
  }
}