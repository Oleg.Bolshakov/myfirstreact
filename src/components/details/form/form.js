import React, { Component } from 'react';
import './form.css';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as imageActions from '../../../actions/ImageActions.js';

class Form extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      comment: '',
      sending: false
    };
  }

  onSubmit = (event) => {
    event.preventDefault();
    const { addComment } = this.props.imageActions;
    const imageId = this.props.imageId;
    let url = `https://boiling-refuge-66454.herokuapp.com/images/${imageId}/comments`;
    this.setState({sending: true});
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: JSON.stringify({...this.state})
    }).then(result => {
      console.log(result);
      addComment({
        id: imageId,
        text: `${this.state.name}: ${this.state.comment}`
      });
      this.setState({
        name: '',
        comment: ''
      });
    }).catch(error => {
      console.log(error);
    }).finally(() => {
      this.setState({sending: false});
    })
  }

  onChangeHandler = (event) => {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  render() {
    const sending = this.state.sending;
    return (
    <div className="form-container">
      <form onSubmit={this.onSubmit}>
        <input
          type="text"
          name="name"
          placeholder="Your name.."
          onChange={this.onChangeHandler}
          value={this.state.name}
          required
        />
        <textarea
          type="text"
          name="comment"
          placeholder="Your comment.."
          onChange={this.onChangeHandler}
          value={this.state.comment}
          required
        />
        <button
          type="submit"
          disabled={sending}
        >{sending ? (<i className="fa fa-spinner fa-spin"></i>) : (null)}Submit</button>
      </form>
    </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    images: state.images
  }
}

function mapDispatchToProps(dispatch) {
  return {
    imageActions: bindActionCreators(imageActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Form);