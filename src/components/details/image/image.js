import React, { Component } from 'react';
import './image.css';

export default class Image extends Component {

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
    <div>
        <img src={this.props.url} alt="Loading..." width="600" height="400" />
    </div>
    );
  }
}