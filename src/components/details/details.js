import React, { Component } from 'react';
import './details.css';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as imageActions from '../../actions/ImageActions.js';

import Image from './image/image.js';
import Comments from './comments/comments.js';
import CommentForm from './form/form.js';

class Details extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      image: null
    };
  }

  componentDidMount() {
    const { addImage } = this.props.imageActions;
    const url = 'https://boiling-refuge-66454.herokuapp.com/images/' + this.props.imageId;
    fetch(url)
      .then(res => res.json())
      .then(result => {
        addImage({...result});
      }).catch(error => {
        this.setState({error: error});
      }).finally(() => {
        this.setState({isLoaded: true});
      })
  }

  render() {
    const { error, isLoaded } = this.state;
    const { image } = this.props.images;
    if (error) {
      return <div>Ошибка: {error.message}</div>;
    } else if (!isLoaded) {
      return <div className="loader">Loading...</div>;
    } else {
      return (
        <div className="grid-container">
          <div className="grid-item image">
            <Image url={image.url} />
          </div>
          <div className="grid-item comments">
            <Comments comments={image.comments}/>
          </div>
          <div className="grid-item comment-form">
            <CommentForm imageId={image.id}/>
          </div>
        </div>
      );
    };
  }
}

function mapStateToProps(state) {
  return {
    images: state.images
  }
}

function mapDispatchToProps(dispatch) {
  return {
    imageActions: bindActionCreators(imageActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Details);