import React, { Component } from 'react';
import './modal.css';

export default class Modal extends Component {

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const show = this.props.show;
    if (show) {
      return (
        <div className="modal">
          <div className="modal-content">
            <div className="close" onClick={e => {this.props.onClose(e);}}>&times;</div>
            <div>{this.props.children}</div>
          </div>
        </div>
      )
    } else {
      return null;
    }
  }

}