import React, { Component } from 'react';
import './images.css';
import Modal from '../modal/modal.js';
import Details from '../details/details.js';

export default class Images extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      images: [],
      imageId: null
    };
  }

  componentDidMount() {
    fetch("https://boiling-refuge-66454.herokuapp.com/images")
      .then(res => res.json())
      .then(result => {
        this.setState({images: Array.isArray(result) ? result : []});
      }).catch(error => {
        this.setState({error: error});
      }).finally(() => {
        this.setState({isLoaded: true});
      })
  }

  onShow (id) {
    document.body.style.position = 'fixed';
    this.setState({
      imageId: id,
      show: true
    });
  };

  onClose = e => {
    this.setState({
      show: false
    });
    document.body.style.position = '';
  };

  render() {
    const { error, isLoaded, images, show, imageId } = this.state;
    if (error) {
      return <div>Ошибка: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Загрузка...</div>;
    } else {
      const imageItems = images.map((image) =>
        <div className="responsive" key={image.id.toString()}>
          <div className="gallery">
            <a href="/#" onClick={() => this.onShow(image.id)}>
              <img src={image.url} alt={`Loading ${image.id}`} width="300" height="200"/>
            </a>
          </div>
        </div>
      );
      return (
        <div className="container">
          <div>{imageItems}</div>
          <Modal show={show} onClose={this.onClose}>
            <Details imageId={imageId}/>
          </Modal>
        </div>
      );
    };
  };
}
