import React from 'react';
import './App.css';

import Images from './components/images/images.js';

function App() {
  return (
    <div className="App">
      <h1>TEST APP</h1>
      <Images />
    </div>
  );
}


export default App;
